const { Gitlab } = gitlab;
const api = new Gitlab();

// queries GitLab for the user's GitLab ID
async function getID(username) {
	let user = await api.Users.all({
		username: username
	});
	return user[0].id;
};

// queries GitLab using given user ID for user information
async function getInfo(userID) {
	let info = await api.Users.show(userID);
	return info;
};

function changeInnerByClassName(className, inner) {
	elements = document.getElementsByClassName(className);
	for (var i = 0; i < elements.length; i++) {
		elements[i].innerHTML = inner;
	}
}

// replaces information on page with information gathered
function fillUserInfo(userObject) {
	var username = userObject.username;
	var bio = userObject.bio.substring(0, 300);	// truncate biography

	changeInnerByClassName(username + "-name", userObject.name);
	document.getElementById(username + "-avatar").src = userObject.avatar_url;
	document.getElementById(username + "-bio").innerHTML = bio;
	document.getElementById(username + "-location").innerHTML = userObject.location;
	document.getElementById(username + "-si-website").href = "http://" + userObject.website_url;
	document.getElementById(username + "-si-gitlab").href = userObject.web_url;
	document.getElementById(username + "-si-linkedin").href = "https://linkedin.com/in/" + userObject.linkedin;
}

async function getGitLabInfo(username) {
	let userID = await getID(username);
	let userInfo = await getInfo(userID);
	fillUserInfo(userInfo);
}

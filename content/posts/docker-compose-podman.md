---
title: Replacing Docker Compose with Podman (Debian)
profileSource: gitlab
authors: ["jallbrit"]
tags: ["hosting"]
date: 07 May 2021
---

## Why Podman?

If you host anything using Docker, you've probably heard of [podman](https://podman.io/), which is advertised as a drop-in replacement for Docker with the advantages of being daemonless and rootless (except for its docker-compose functionality, which we will get into). DYOR, but podman is a very appealing and future-oriented alternative to Docker.

Fortunately for us, [podman v3.0.0](https://github.com/containers/podman/releases) included initial support for Docker Compose, which we will be using in this guide. The podman community is also great, and `#podman` was very helpful with the transition from docker-compose.

The majority of [existing guides](https://www.redhat.com/sysadmin/podman-docker-compose) are limited to Fedora/RHEL and don't work on Debian, so I wrote this guide specifically for those looking to replace Docker Compose with podman on Debian.

## Replace Docker Compose with Podman

### 1. Install Podman

First, we need to install podman (duh). Podman is only available via the Bullseye (Debian 11) repository, and as of today (May 7, 2021), this requires upgrading to Debian testing. Or, you can try to install podman via source, but this guide won't cover that.

```bash
$ sudo apt install podman
```

### 2. Swap Services

Before stopping docker altogether, go through and `docker-compose stop` all your services, until `docker ps` shows no running containers, then continue.

Essentially, docker uses two systemd services, `docker.service` and `docker.socket`. In a similar fashion, podman provides `podman.service` and `podman.socket`. The docker socket is available (on Debian) at `/var/run/docker.sock`, and the podman socket is at `/run/podman/podman.socket`. These locations will be important in a minute.

Next, we need to swap the services that we're running:

```bash
$ sudo systemctl disable --now docker.service docker.socket
$ sudo systemctl enable --now podman.service podman.socket
```

To verify that the podman socket is listening, you can run the following:

```bash
$ sudo curl -v --unix-socket /run/podman/podman.sock http://localhost/_ping
```

You should see `OK` at the end.

### 3. Set up Container Registry List

Unlike Docker, which defaults to `docker.io`, podman does not prefer any specific container registry list by default. It must be configured at `/etc/containers/registries.conf` so it knows to grab containers from `docker.io`. You can read more about this file [here](https://www.redhat.com/sysadmin/manage-container-registries), but I'll give you the easy version.

Open up the file and add the following line, or change the commented line that looks similar:

<div class="codeblock-label">/etc/containers/registries.conf</div>

```yaml
unqualified-search-registries = ['docker.io']
```

If you want your container images from somewhere else, just add a different URL.

### 4. Run Docker Compose

At this point, if you were to try and run `docker-compose up`, you'd get the following error:

```bash
$ docker-compose up -d
ERROR: Couldn't connect to Docker daemon at http+docker://localhost - is it running?

If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.
```

Essentially, we just shut down the Docker socket, so `docker-compose` doesn't know where to go. From this point forward, if you want to run a `docker-compose` command, you must prepend the command by pointing it to the podman socket via the `DOCKER_HOST` environment variable, just like the error message suggested.

Now, let's bring up the containers again, this time using `podman`'s socket:

```bash
$ sudo DOCKER_HOST=unix:///run/podman/podman.sock docker-compose up -d
```

You should see podman creating networks and volumes, pulling images, and creating containers just like docker did.

## Conclusion

Boom, you just dropped Docker and swapped to Podman. Pretty easy, right? It gets better: in an upcoming release, podman is dropping the root requirement for `docker-compose`, so look forward to an update!

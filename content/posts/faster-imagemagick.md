---
title: How to Make ImageMagick Faster (Q8 Mode)
authors: ["jallbrit"]
tags: ["linux"]
date: 2024-08-17
---

If you've ever edited images from the CLI, you've probably used [ImageMagick](https://imagemagick.org/index.php).

> ImageMagick® is a free, open-source software suite, used for editing and manipulating digital images. It can be used to create, edit, compose, or convert bitmap images, and supports a wide range of file formats, including JPEG, PNG, GIF, TIFF, and Ultra HDR.

> ImageMagick is widely used in industries such as web development, graphic design, and video editing, as well as in scientific research, medical imaging, and astronomy. Its versatile and customizable nature, along with its robust image processing capabilities, make it a popular choice for a wide range of image-related tasks.

ImageMagick is incredibly useful, but if you're processing especially large images, or using a low-power machine or server, image operations can take too long. Fortunately, there's a way to fix that-- but, it will involve re-building ImageMagick from source using Q8 mode.

## Q8 Mode?

By default, ImageMagick is in Q16 mode, which means each pixel has 16 bits of storage for its color-- that's 2^16, or 65,536 unique colors. If you're like me and you don't care about that level of quality for some specific applications, you may be interested in ImageMagick's Q8 mode, which only uses 8 bits per pixel-- that's 2^8, or 256 unique colors.

> For handling any sort of large image it would probably be better for you to use a Q8 version of ImageMagick, which has half the memory requirements of the higher quality Q16 version. Check your IM's compiled Q level using "identify -version".
> <br>-- [ImageMagick Examples](https://legacy.imagemagick.org/Usage/files/#massive)

### 1. Install Dependencies

Since we're going to build ImageMagick from source, we need some dependencies-- primarily, libraries for handling `PNG` and `JPG` files, since these are two of the most common types of images you might want to edit.

On Debian, it's easy:

```
$ sudo apt install libpng-dev libjpeg-dev
```

### 2. Clone the Repo

Go to the [ImageMagick releases page](https://github.com/ImageMagick/ImageMagick/releases), and take note of the latest version (e.g. `7.1.1-36`). Use that version string to set the first command:

```
$ VERSION_STRING=7.1.1-36
$ git clone --depth 1 --branch "$VERSION_STRING" https://github.com/ImageMagick/ImageMagick.git ImageMagick-"$VERSION_STRING"
$ cd ./ImageMagick-"$VERSION_STRING"
```

### 3. Configure

Next is configuring the build. There are a lot of options-- you can see them all with `./configure --help`, but we're only interested in one option. This step could take some time:

```
$ configure --with-quantum-depth=8
```

### 4. Build

The most time-and-resource intensive part is actually compiling and linking everything, which happens during the `make` step. If you're running this on a machine with low resources, you may get cryptic errors-- try stopping other RAM-intensive programs.

```
$ make
```

### 5. Install

Lastly, you have to install the new binaries and libraries into the correct parts of your system. To my knowledge, it's not possible to install without sudo-- to quote the [build instructions from ImageMagick](https://www.imagemagick.org/script/install-source.php), "Administrator privileges are required to install."

```
$ sudo make install

(this command may not be necessary on your system-- but it was necessary for mine)
$ sudo ldconfig /usr/local/lib

# you should see "Q8" in version:
$ identify -version
```

That's it! You should be able to run `magick` commands now. If you're stuck, check out the official ["Install from Source" instructions](https://www.imagemagick.org/script/install-source.php).

## Sources

* https://www.imagemagick.org/script/install-source.php
* https://legacy.imagemagick.org/Usage/files/#massive

---
title: Normalizing Audio in Linux
profileSource: gitlab
authors: ["jallbrit"]
tags: ["linux"]
date: 2019-02-21
---

## Audio Normalization, Explained

*Audio normalization* is the process of applying an audio gain to music so that the average sound volume reaches a target level. In my case, I had multiple albums of soundtracks from all different sources, including video games and TV shows, with varying levels of volume. This was particularly annoying when I had my collection on shuffle, having to constantly change the audio level to fit the particular song. Audio normalization takes care of this so that you can listen at a relatively stable volume.

> Warning: it is always a good idea to backup your music files before attempting to normalize them.

## Methods

There are two levels at which normalization can be applied:

* By song: each track is treated individually. If a song is particularly quiet compared to the rest of its album, the normalizer doesn't care and will bring the song up to the desired volume level, thus possibly destroying the music composer's intentions of keeping that song quiet.
* By album: each track is considered as part of a collection/album. The normalizer will determine a gain for the *entire album* and apply that same gain to each song.

Most normalizers will work by song by default. In my case, I needed one with album functionality.

## The Competition

Each of these normalizers work by editing the `ReplayGain` tag in mp3 file headers. This means that they are *lossless* normalizers, so the quality of audio files is *not* affected. However, the drawback is that your music player needs to be able to see these header changes. Some music players will ignore this and the normalization will have no affect. For each competitor, I will test using the `Clementine` gui music player. Performance/speed is not of great importance to me, since normalization of my music is something that will likely occur only once per album, ever.

### normalize-audio

`normalize-audio` (available via `APT`) uses [RMS amplitudes](http://normalize.nongnu.org/README.html) to determine volume; I noticed that this algorithm tends to make large gain changes to normalize songs. It supports file and album normalization and has lots of CLI options, including a preview of changes *before* they take place. However, `Clementine` was unable to recognize the changes made. 

### mp3gain

`mp3gain` (available from their [homepage](http://mp3gain.sourceforge.net/)) uses [a Replay Gain algorithm](http://mp3gain.sourceforge.net/faq.php)  to determine volume, which tends to make smaller gain changes to normalize. It definitely comes with the most customizability as far as options go, however only supports *integer* gains on music, which may or may not be a big deal for you. It supports file and album normalization. `Clementine` was able to recognize the changes made. `mp3gain` was removed from the `APT` repo and replaced with an alternative `replaygain`.

### replaygain

`replaygain` (available via `APT` as `python-rgain`) is a `python` audio normalizer. It supports file and album normalization, as far as I can tell, but the documentation almost seems to suggest that *both* are applied simultaneously. `Clementine` was able to recognize the changes made.

## Conclusion: mp3gain

Simply due to its multitude of options and superior documentation, `mp3gain` takes the win.

---
title: Setting Up a New Storage Device
profileSource: gitlab
authors: ["jallbrit"]
tags: ["linux"]
date: 2020-07-16
---

## Introduction

Let's say you've just gone to the store to pick up a new HDD or SSD for your computer or server. Once plugged in, the **block storage device** must go through a series of steps before it can be used to read and write data from. This tutorial will guide you through those steps.

This tutorial will follow a couple rules:

1. Assume the user cannot access the target computer's graphical interface
2. Avoid installing additional software; instead, use tools that come default

## Partitioning

*Partitioning* is the process of separating a storage device into separate regions of disk space that can then be managed separately. For example, a fresh Linux installation will create at least 2 partitions: one for your main operating system to read and write files from, and another called *swap* that is used as additional memory when your computer's memory is running low.

Before formatting a hard drive to contain a filesystem, we must partition it. On Linux, there are multiple tools to accomplish this, but we will be using `fdisk` as it is likely already installed on your target machine.

### 1. Identify Target Block Storage Device

If you have more than 1 block storage device plugged in (if you're reading this tutorial, you probably do), you need to first find out which one it is you're trying to set up.

Run `fdisk -l` to list the partition tables for all devices. Here's an example of what it may look like:

```bash
Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x12345678

Device     Boot    Start      End  Sectors  Size Id Type
/dev/vda1  *        2048 39845887 39843840   19G 83 Linux
/dev/vda2       39847934 41940991  2093058 1022M  5 Extended
/dev/vda5       39847936 41940991  2093056 1022M 82 Linux swap / Solaris


Disk /dev/sdx: 256 GiB, 274877906944 bytes, 536870912 sectors
Disk model: EXAMPLE
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```

In this example, we have two disks plugged in, `/dev/vda` and `/dev/sdx`. As you can see, `/dev/vda` already contains 3 partitions, and `/dev/vda1` is was booted from, as it contains a `*` in the `Boot` column. I also know the new device has a 256 GB capacity, so it's easy to tell that `/dev/sdx` is the device I want to partition.

> Note: each machine will have different names for the attached devices. I will be using `sdx` in the tutorial, but **make sure** you are using the correct device label since we will be wiping data on the target device.

### 2. Enter `fdisk` Command Prompt

Now that we know our target device is `sdx`, let's partition the drive using `fdisk`:

```bash
$ sudo fdisk /dev/sdx

Welcome to fdisk (util-linux X.XX.X).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x12345678.

Command (m for help): 
```

This will open up a new `fdisk` command prompt where we can enter in `fdisk` commands to alter the drive. Don't worry, nothing you do is written to the device until you enter `w`. Just like the prompt tells us, let's type `m` for help:

```bash
Command (m for help): m

Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table
```

### 3. Create New Partition

Since we want to `add a new partition`, the command we're looking for is `n`. `fdisk` will ask for the type of partition and partition number, and hitting enter twice for the defaults (`primary` and `1`) is fine in most cases. `fdisk` then asks for the first and last sectors, which set the bounds of this partition within the drive. Assuming you want this partition to take up the whole drive space, hitting enter twice again is fine.

```bash
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1):
First sector (2048-536870911, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-536870911, default 536870911):

Created a new partition 1 of type 'Linux' and of size 256 GiB.
```

### 4. Write Changes and Exit

Now that the partition is created, type `w` to write the changes to the disk and exit `fdisk`. If you want to see the partition table before making changes, use `p`. You should see the new partition under the target device.

## Formatting

Formatting a disk prepares it for use; in our case, that means establishing a file system on the device, since we later want to read and write data to it. There are a myriad of file systems available for use, including Window's `NTFS`, `exFAT` commonly used in USB sticks, and `ext4` commonly used for Linux. Decide which filesystem to use before continuing, but for this tutorial we will use `ext4` since it is great (and typically default) for Linux usage.

### 1. Identify Target Partition

Now that we have partitioned our target device, it's important we know *which partition* on the device to format with a filesystem. Run `fdisk -l` once again to see a list of devices and partitions:

```bash
$ sudo fdisk -l
Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x12345678

Device     Boot    Start      End  Sectors  Size Id Type
/dev/vda1  *        2048 39845887 39843840   19G 83 Linux
/dev/vda2       39847934 41940991  2093058 1022M  5 Extended
/dev/vda5       39847936 41940991  2093056 1022M 82 Linux swap / Solaris


Disk /dev/sdx: 256 GiB, 274877906944 bytes, 536870912 sectors
Disk model: EXAMPLE
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb6024598

Device     Boot Start       End   Sectors  Size Id Type
/dev/sdx1        2048 536870911 536868864  256G 83 Linux
```

> Don't get confused: in the output of `fdisk -l`, the column that says `Device` is really a list of partitions.

Notice that there is a new listing of `Device`'s under `/dev/sdx`. Since I only created 1 partition earlier, I only see 1 partition `/dev/sdx1`, but every machine is different and your target device may have multiple partitions.

> Remember: there is a difference between our target **device**, `sdx`, and our newly created target **partition**, `sdx1`.

### 2. Format Target Partition

`mke2fs` is a standard Linux tool to create `ext` filesystems. To format our target partition `sdx1` with `ext4`, use:

```
$ sudo mke2fs -t ext4 /dev/sdx1
```

After some time, the command will exit. To ensure the partition was formatted successfully, you can run `lsblk -f` or `df -T` to see a list of partitions and filesystems.

## Mount the Filesystem

Formatted partitions must be *mounted* to a specific directory in the current, running filesystem in order to read or write data. The `/mnt/` directory is designed to contain mounted filesystems. First, create a directory to contain the mounted filesystem (I'll call this directory  `device1`), then mount it using `mount`:

```
$ sudo mkdir /mnt/device1
$ sudo mount /dev/sdx1 /mnt/device1
```

Now, we can read and write data using `/mnt/device1` and that data will be read and written to the device.

## (Optional) Create a Persistent Mount

Even though `mount` will mount the target filesystem, it is not *persistent*, meaning we will lose the mount when the machine is rebooted and `mount` must be run again. However, Linux has a specific service called `fstab` that automatically mounts filesystems upon boot.

To create a persistent mount, we need to edit `fstab`'s configuration file at `/etc/fstab`. Open it using your favorite editor:

```
$ sudo vim /etc/fstab
```

```
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/vda1 during installation
UUID=0c8f8eb2-67fa-41e4-93a2-156ff0e97fdf /               ext4    errors=remount-ro 0       1
# swap was on /dev/vda5 during installation
UUID=b7184019-83ec-4d8d-8558-74bc414993b5 none            swap    sw              0       0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
```

Each line in this file follows a specific 6-column format that tells `fstab` how to mount each filesystem. Regardless of how different your `/etc/fstab` may look, we need to add a new line at the end of the file containing the following:

```
/dev/sdx1       /mnt/device1    ext4    defaults        0       2
```

Essentially, this line tells `fstab` to mount `/dev/sdx1` to `/mnt/device1` as an `ext4` filesystem, using default `mount` options, don't dump the filesystem, and to perform a `fsck` (file system check) on the filesystem after checking the primary filesystem.

> Note: To really understand what each column means, check out the `fstab` manual using `man fstab`.

## Conclusion

In this tutorial, we partitioned a new storage device, formatted a partition to a filesystem, and created a persistent mount so that we can always access the filesystem at a specific place.

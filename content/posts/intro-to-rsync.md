---
title: Intro to Rsync
subtitle: Or, How I Manage my Music Files
profileSource: gitlab
authors: ["jallbrit"]
tags: ["backups"]
date: 2020-03-18
---

## Introduction

> rsync - a fast, versatile, remote (and local) file-copying tool \
> --<cite>rsync man page</cite>

As the man page suggests, rsync is a useful tool if you're tired of `cp`-ing files around servers and managing the disastrous permissions aftermath. It has the ability to manage permissions, compress on-the-fly, and doesn't waste time copying things that already exist in the destination.

## Rsync is Smart

Let's say you have two folders, `folder1` and `folder2`. These folders contain a very large amount of data, and right now they are the exact same. But let's say you make a small change to `folder1`, like maybe adding a small file, or fixing a typo somewhere.

Without rsync, to ensure `folder2` still contains an accurate copy, you have a couple options:

* **Manually change `folder2` the same way you did to `folder1`.** To do this, you'd have to remember *exactly* what you did to `folder1` each time you make a change. What happens when you have 4 or 5, or 100 copies of this folder? Assuming you had perfect memory and a lot of time, this solution still isn't perfect, because the modification times on the files would be different.
* **Remove `folder2` and re-copy `folder1`.** This would work, if the folders were small-- but for our example, they're not. `cp`-ing this folder could take hours.

Rsync is smart in that it only copies *changes* to your files. If we added `one-small-file.txt` to `folder1` and ran rsync, it would only add `one-small-file.txt` to `folder2` and call it a day- no need to re-copy the existing accurate files.

## Example Use Case: Copying Music

I run an [Airsonic music server](/posts/setup-airsonic-server/), which holds the "master copy" of some music I listen to. However, I prefer to keep local copies of this music on some of my machines, and so I find myself constantly having to keep all versions of this music the same across all my machines and server.

> **What about Git?**
> Git is a version control system that would appear to solve this problem of concurrency; however, [git works poorly for this use case](https://superuser.com/questions/13376/using-git-to-manage-an-itunes-library).

With a single `rsync` command on each machine, I can pull down changes to the music folder to my local copy. This way, the copy on the server is the only one I have to manage.

```bash
rsync --delete --recursive --compress --verbose user@domain.com:/remote/music/ /local/music/
```

## Conclusion

Rsync is a versatile tool, and has a ton of capabilities I didn't cover in this introduction. `man rsync` covers these and more.

---
title: Using a Thesaurus File in Vim
profileSource: gitlab
authors: ["jallbrit"]
tags: ["vim", "tutorial"]
date: 2019-11-21
---

As you use vim more and more, you will uncover new tips and tricks. One such trick is how to use vim's builtin thesaurus functionality to replace words you may find yourself using too much.

## The Thesaurus File

> Each line in the file should contain words with similar meaning, separated by non-keyword characters (white space is preferred).
>
> -- <cite>`:h thesaurus`</cite>

Unfortunately, this is about as much guidance as vim will give us on this one. Essentially, vim wants things in the form of `word {synonym 1} {synonym 2} {synonym 3}...`, like so:

<figure>

```
angry upset mad pissed
happy glad uplifting
throw chuck push
```

<figcaption>Notice there are no multiple-word synonyms like "happy full of joy".</figcaption>

</figure>

However, I was unable to find any thesaurus files online to fit this pattern. What I was able to find was several files in the following format:

<figure>

```
angry,acid,acrimonious,aggravated,algetic,anarchic,angered,annoyed
happy,a propos,accepting,accidental,ad rem,adapted,addled,advantageous
throw,abandon,addle,agitate,amaze,apply,assume,baffle,bake
```

<figcaption>

This [vim thesaurus plugin](https://github.com/Ron89/thesaurus_query.vim) has plenty of links to thesaurus files like this.

</figcaption>
</figure>

## The Whitespace Lie

Remember what `:h thesaurus` said about thesaurus files?

> white space is preferred

Well, this isn't entirely true. After looking at some [GitHub issues](https://github.com/vim/vim/issues/1611), it seems that whitespace is the *only* delimiter that vim allows for thesaurus files.

At this point, we have two options: try and twist our `mthesaur.txt` into something vim can read, or write our own thesaurus functionality. Luckily for us, someone has [already written a function](https://www.reddit.com/r/vim/comments/55y53e/allow_spaces_in_thesaurus_entries/) that allows us to use this format:

```vim
function! s:thesaurus()
    let s:saved_ut = &ut
    if &ut > 200 | let &ut = 200 | endif
    augroup ThesaurusAuGroup
        autocmd CursorHold,CursorHoldI <buffer>
                    \ let &ut = s:saved_ut |
                    \ set iskeyword-=32 |
                    \ autocmd! ThesaurusAuGroup
    augroup END
    return ":set iskeyword+=32\<cr>vaWovea\<c-x>\<c-t>"
endfunction

nnoremap <expr> <leader>t <SID>thesaurus()
```

Throw this snippet into your `~/.vimrc` (or `init.vim`, whatever suits your fancy) and we've got ourselves a handy-dandy thesaurus! Let's break down that last line:

* `nnoremap`: normal mode, non-recursively remap
* `<expr>`: emulate hitting enter after mapping is completed
* `<leader>t`: the binding is called with `<leader>t` (`leader` defaults to `\`)
* `<SID>thesaurus()`: call the unique function `thesaurus()`

Now that we've gathered a thesaurus file and added the new function, all that's left is to press `\t` in normal mode over the word you'd like to replace.

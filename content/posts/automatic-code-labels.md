---
title: Automatic Code Block Labels using Javascript
profileSource: gitlab
authors: ["jallbrit"]
tags: ["markdown"]
date: 2019-11-29
---

In [another post](https://thesynack.com/posts/codeblock-labels-markdown/), we explored adding custom labels to code blocks to give them some context, like a filename or language.

In Markdown, the language of a code block can be specified like so:

````markdown
```python
import time

print(2 + 2)
```
````

Among other things, this allows Markdown processors to apply syntax highlighting to the code blocks. Since we're already specifying the language, wouldn't it be helpful to add an automatic code label with the language of the code we're looking at?

## JavaScript Saves the Day

Here's a snippet of JS I have written to work with Hugo, the static site generator I use to create this website.

```javascript
var highlightBlocks = document.querySelectorAll(".highlight");

for( i=0; i < highlightBlocks.length; i++) {
	var highlightBlock = highlightBlocks[i];

	// get language data
	var codes = highlightBlock.querySelectorAll("[data-lang]");
	var lang = codes[0].getAttribute("data-lang");

	// check if custom label is already made
	if (highlightBlock.previousElementSibling.classList.contains("codeblock-label")) continue

	// create label element
	var label = document.createElement("p");
	label.classList.add("codeblock-label");
	label.innerHTML = lang;

	// insert label
	highlightBlock.parentNode.insertBefore(label, highlightBlock);
}
```

This JavaScript snippet finds all instances of syntax highlighting, figures out what language the code block is written in, ensures a [custom label](https://thesynack.com/posts/codeblock-labels-markdown/) hasn't been written, and then adds the label containing the language.

CSS can then be applied to the label with the `.codeblock-label` selector.

You can see it in action on the above code block!

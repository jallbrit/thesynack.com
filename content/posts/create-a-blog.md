---
title: How to Create Your Own Website or Blog
profileSource: gitlab
authors: ["jallbrit"]
tags: ["hugo", "tutorial"]
date: 2019-09-17
---

## Overview

Creating a personal website or blog can be as simple or as complex as you want it to be. There's a wide range of solutions, and no single one works for everybody. You have to decide for yourself what your goals are and how much time, effort, and money to put into it. You've got to think about the technical details like `html`, how your website should be organized, and how authors or editors are going to publish content on your site.

Thankfully, there are more than enough resources available for any experience or funding level-- it's entirely possible to get a site started and pay *nothing*. That being said, let's get started.

## Domain Name

A domain name is the thing you see at the top of your browser, and it points to a particular website like [google.com](https://google.com) or [example.org](example.org). A good domain name will distinguish your content, possibly give your site a theme, and most importantly, give your members a name to come back to.

If you don't mind using a subdomain (like [mycoolwebsite.weebly.com](#)), there are several places to get one for free. For example, creating a website at [weebly.com](https://weebly.com) will get you a subdomain at [yourdomainhere.weebly.com](#). This is fine for small sites.

If you want a *custom* domain name, like [mycoolidea.com](#), you're going to have to pay a domain registrar like [Namecheap](namecheap.com) or [Hover](hover.com). These registrars charge based on the TLD (.com, .org, .xyz, etc.), and a .com domain will run you around 10-20 USD/year. While you're shopping, I highly recommend you find a registrar that offers *whois* privacy, which protects your personal data from being publicly tied to your domain.

## Creating Site Content

One way or another, you have to create the layout and content of your website, like how the navigation bar looks, what the footer says, and what color the text is.

### WYSIWYG

What the heck does that mean? Well, a What You See is What You Get (WYSIWYG) builder is like the training wheels for website-building.

Sites like [Weebly](weebly.com) or [Wix](wix.com) have dead easy drag-and-drop editors to build your website's content, and even offer free subdomains. However, they're going to cost you for premium features or analytics, and restrict your control over what your site can look like. Regardless, they're still wildly popular and may be worth giving a try if you aren't tech-savy or don't have a lot of time.

### Writing your own website

<figure>
	<img src="https://i.stack.imgur.com/DActu.png">
	<figcaption>who wants to write <em>this</em>?</figcaption>
</figure>

Websites are made of 3 main languages: `html`, `css`, and `javascript`. If you're willing to put in lots of time and effort, it's certainly possible to create your own website by hand. However, this takes lots of experience that most people don't have and don't want to learn-- including myself. Thankfully, there's a simpler option: SSGs.

### Static Site Generators

Static Site Generators (SSGs) are a bit of a middle ground. They do require time and knowledge to use and maintain, but they don't come **close** to the resources required to write your own website from scratch. 

Since there's no risky server-side scripting going on, the security risks are extremely low. 

Essentially, you write content in a markup language of your choosing, (for compatibility purposes, most SSGs will take `markdown`) and the SSG will convert this easy-to-write text into fancy, beautiful html for your web host to serve.

This makes writing content *drastically* easier-- `markdown` is an *easily* obtained skill. It takes about 60 seconds to master the basics. For example, if you want to put text in italics, just surround it in asterisks like this: `*hello*` becomes *hello*. But one of your authors or editors might not prefer writing in this language: that's where a Content Management System comes in.

## Web Hosting

Once you've got the files for your website, however you've created them, you need a web server. When a user types in mydomain.com in their browser, a web server is what gives them your website.

### Self-Hosted

If you want total control over your web server, including initial setup, ongoing configuration, and security updates, you're going to want to run your own web server.

A website like [DigitalOcean](digitalocean.com) will rent you a Virtual Private Server (VPS) that is essentially your own machine that you can manage-- except, unlike your personal computer, you can install services on it that *anyone* on the Internet can access.

### Free SSG Hosting

If you're using a popular SSG, there are services like GitHub Pages, GitLab Pages, or Netlify that will host your website for *free*. Anytime you push updates to your site's code, the service will automatically re-run the SSG and push out the site with the new content. These services are very popular, and for a good reason-- they're free and easy to use.

## Content Management System

CMSs provide an easy, graphical environment to write text. It's the equivalent of using Microsoft Word to write an essay. There are [several solutions](https://headlesscms.org/) out there. A CMS will provide a way for editors to log in and create their content on your site. If it's just you creating content, a CMS might not be necessary: it all depends on your needs.

CMSs are available a few different ways. You can pay a service like Ghost(Pro) to host a CMS for you, or you can host the CMS yourself. There are also some websites such as [prose.io](prose.io) that will hook up to your GitHub Pages website and serve as a free CMS.

## Analytics

If you want analytics on how users interact with your site, such as how users found your site, what links or advertisements they click on, or their demographics, you're going to need an analytic service. 

The most popular option, which is also free, is Google Analytics. It's very easy to add this feature to an existing site, but it can be absolutely crucial for some types of websites, like ecommerce.

However, if you don't care about attracting or maintaining users, then analytics might not do anything for you.

## A Common Theme

No single solution I've outlined works for everything. In fact, there are tons of resources and methods not even mentioned. The point is, figure out what technologies work best for *you*.

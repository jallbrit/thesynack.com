---
title: The History of Linux Control Groups
date: 11 Aug 2020
authors: ["jallbrit"]
profileSource: gitlab
tags: ["linux"]
draft: true
---

## systemd

[systemd](https://systemd.io) is a common init system that is responsible for tasks like starting services and managing all processes. Although other init systems exist to serve the same purpose, systemd is far and away the most common on a majority of modern Linux machines.

Part of systemd's responsibilities includes starting and managing system services, like `journald`, `cron`, `NetworkManager`, and many more. systemd allows users to configure the resource usage of each of these services, and then implements those controls using cgroups.

## CGManager

> CGManager is a ... daemon to manage control groups (cgroups) on a Linux system ... it is mostly targeted at doing that management for LXC containers, but it was originally envisioned as an alternative to systemd's cgroup management for those distributions that were not using systemd as their init.
>
> --<cite>[Jake Edge, "A control group manager"](https://lwn.net/Articles/618411/)</cite>

CGManager was originally created to simplify cgroup management for programs and services, and to work on any init system. More specifically, it was created to work with Linux Containers (LXC) in order to keep containers' processes separated. cgmanager worked by exposing a D-Bus interface to manage cgroups.

Eventually, the Linux kernel started supporting isolated namespaces, and the [need for cgmanager was no longer present](https://s3hh.wordpress.com/2016/06/18/whither-cgmanager/). The `cgmanager` project was closed in 2016 and never made it to cgroups v2.

---
title: Setting Up an Airsonic Server
profileSource: gitlab
authors: ["jallbrit"]
tags: ["tutorial", "airsonic"]
date: 2020-03-17
---

## What is Airsonic?

[Airsonic](https://airsonic.github.io/) is a free, open source music server. As an admin of an Airsonic server, I upload music I've already tagged with [Beets](http://beets.io/), and Airsonic allows me and my friends to stream the music to our devices.

Other popular music servers such as [Funkwhale](https://funkwhale.audio/) exist, and you can even stream using [mpd on a server](https://feeding.cloud.geek.nz/posts/home-music-server-with-mpd/). However, the simplicity, features, and clients of Airsonic appealed to my needs.

That being said, we will now begin installing Airsonic.

## 1. Create an Airsonic user

It's generally a good rule of thumb to minimize processes running as root, so you'll want to create a separate `airsonic` user who will manage the Airsonic files and own the process:

```bash
$ sudo useradd --shell /bin/false -m -d /home/airsonic airsonic
```

This command creates the user `airsonic`:

* With the `false` shell, which prevents users from logging into the `airsonic` account
* With a home directory at `/home/airsonic`, which is where we will place Airsonic files

## 2. Run the Docker Container

Although there are [multiple ways to install](https://airsonic.github.io/docs/install/source/) Airsonic, we will use the [Docker method](https://airsonic.github.io/docs/install/docker/) for this tutorial. To get this ~~party~~ container started, you can pull, run, and forget the Docker container all in one command:

```bash
$ docker run \
	-v /home/airsonic/data:/home/airsonic/data \
	-v /home/airsonic/music:/home/airsonic/music \
	-v /home/airsonic/playlists:/home/airsonic/playlists \
	-v /home/airsonic/podcasts:/home/airsonic/podcasts \
	-p 4040:4040 \
	-u "$(id -u airsonic):$(id -g airsonic)" \
	-e AIRSONIC_DIR=/home/airsonic \
	--restart unless-stopped
	-d \
	airsonic/airsonic
```

This command pulls from the [airsonic/airsonic Docker image](https://hub.docker.com/r/airsonic/airsonic) with some important options. Let's break it down:

* We want all traffic sent to our server at port `4040` to be forwarded to the container's port `4040`
* `-d` detaches our terminal from the container so it can continue running once we close our terminal

### Permissions

Permissions within the Docker ecosystem is a [fairly complicated](https://stackoverflow.com/questions/32397496/docker-replicate-uid-gid-in-container-from-host) topic, but let's go over some basic information.

By default, containers (and commands that run inside of them) will have `root` privileges on the host. Using the principle of least privilege, we want to reduce these privileges. Here's where the `airsonic` user comes in:

```bash
	-u "$(id -u airsonic):$(id -g airsonic)" \
```

We run the container using `airsonic`'s UID and GID, which *don't* have root privileges. This way, if a bug is found inside Airsonic, attackers are only limited to what the `airsonic` user can do- which isn't much.

### Environment Variables

`-e AIRSONIC_DIR=/home/airsonic` passes in an environmental variable so that Airsonic knows where to get its files from. You can see in the `airsonic/airsonic` [Dockerfile](https://github.com/airsonic/airsonic/blob/master/install/docker/Dockerfile) how it uses this variable:

<p class="codeblock-label">airsonic/install/docker/Dockerfile</p>

```bash
VOLUME $AIRSONIC_DIR/data $AIRSONIC_DIR/music $AIRSONIC_DIR/playlists $AIRSONIC_DIR/podcasts
```

<figcaption>The Dockerfile creates 4 volumes using this important variable.</figcaption>

### Docker Volumes

So what about the `-v` lines? Well, we've created 4 [Docker volumes](https://docs.docker.com/storage/volumes/) with the syntax `-v HOST-DIR:CONTAINER-DIR`:

```bash
	-v /home/airsonic/data:/home/airsonic/data \
	-v /home/airsonic/music:/home/airsonic/music \
	-v /home/airsonic/playlists:/home/airsonic/playlists \
	-v /home/airsonic/podcasts:/home/airsonic/podcasts \
```

The 4 folders specified contain important information to run Airsonic, including the music itself. Typically, files created by a container remain inside the container and are destroyed when the container stops. Since we want our configuration and music to be *persistent*, we make the folders into *Docker volumes*.


Making these folders Docker volumes also forces any changes to be immediately reflected between the container and the host. This way, if users create a playlist, it will show up on your server's hard drive instead of only existing inside the container's virtual filesystem.

## 3. (Optional) Initialize a Git Repository

If you plan on keeping track of changes to your Airsonic configuration and database, you'll want to initialize a git repo inside user `airsonic`'s home directory, since that's where the Docker volumes live.

```bash
cd /home/airsonic
git init
git add data music playlists podcasts
git commit -m "Initialize Airsonic"
```

## 4. Configure the Server and Upload Music

Once the Docker container is up and running for a few seconds, you can navigate to `http://your-domain.com:4040` to access the Airsonic web player, which will instruct you how to finish configuration.

Upload music to the folder we specified earlier (`/home/airsonic/music/`), set the media folder under `settings` to that directory, and hit *Scan media folders now*. If you don't see music start to appear in your clients, ensure that your user has the permission to view the media folder.

## Clients

The web player is available for any device with a browser, but there are also [applications available](https://airsonic.github.io/docs/apps/) for both PC and mobile that might offer a better experience.

Once you've found your favorite client (for me it was [play:Sub](https://apps.apple.com/us/app/play-sub-music-streamer/id955329386)), just sit back and enjoy the power of open-source!

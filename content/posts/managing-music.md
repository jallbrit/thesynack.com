---
title: Managing Music with Beets
date: 2020-03-19
profileSource: gitlab
authors: ["jallbrit"]
tags: ["music"]
---

Managing your own music collection can be painstakingly difficult. Downloaded music is often incorrectly labeled, missing songs, and unsorted. The user is left to sort out all these discrepancies. This post will go over *my* solution to managing music.

## Music Tags

Music files contain **tags** that contain song metadata, such as artist, album, genre, and release date. These tags are what equips music players to group songs and help users find what they want to listen to. However, depending on your method of downloading songs, you will find that these tags are often, you guessed it, *wrong*.

I [run an Airsonic server](/posts/setup-airsonic-server/) that holds several days worth of music files (mostly soundtracks), and often come find myself asking questions like whether the album is called *Super Mario 64 OST*, *Super Mario 64 Soundtrack*, or *Super Mario 64 Platinum Hits Soundtrack: Deluxe Edition*.

### Tag Databases

Luckily for us, there are databases that exist to maintain accurate music tags. One such is [MusicBrainz](https://musicbrainz.org), which we will be using in this tutorial. MusicBrainz is community-powered and completely free to use.

## Automatic Music Tagger: Beets

For larger collections, manual tag editors don't cut it. [Beets](https://beets.io) is a CLI automatic tag editor and library manager: when you download music and run it through beets, it will automatically find a matching entry in MusicBrainz, fix the tags, then neatly and intelligently sort your music collection.

### Beets Walk-Through

Beets is highly configurable via both a configuration file and command line options. To begin with, we'll need to edit the configuration file with `beet config -e` and add the following:

```yaml
plugins: fromfilename
directory: ~/Music
library: ~/.cache/beet/musiclibrary.db
```

<figcaption>

Complete configuration documentation is available [here](https://beets.readthedocs.io/en/stable/reference/config.html).

</figcaption>

* `plugins` extend beets' capabilities and are totally optional. It's a good idea to read the [list](https://beets.readthedocs.io/en/stable/plugins/index.html) and see if any would be useful to you.
* `directory` will hold all the actual music files.
* `library` will hold the beets database, which is only important to beets.

Let's say you have some music you'd like to import at `~/Downloads/music`. With beets, it's a simple one-liner to move this music into your library and automatically tag it:

```bash
beet import --autotag --move --timid --group-albums ~/Downloads/music
```

<figcaption>If you're curious what some of these flags mean, check out <code>man beet</code>.</figcaption>

## Contributing to MusicBrainz

As you're tagging your music, you may notice that beets can't find the right tags. You can either [import the music as-is](https://beets.readthedocs.io/en/stable/reference/cli.html#import), or [contribute to the MusicBrainz database](https://musicbrainz.org/doc/How_to_Contribute) so that you and everyone else in the future can benefit.

### Adding a Release using Picard

Although it is possible to manually add albums (MusicBrainz calls them *releases*) to MusicBrainz, there is a much easier method: MusicBrainz's GUI music tagger, [Picard](https://picard.musicbrainz.org/). This tagger is also quite capable, and some may prefer its GUI interface if the CLI scares you.

Picard offers a fast way to contribute releases to MusicBrainz. To use it, go to the plugins settings inside of Picard and install *Add Cluster As Release*.

![installing the plugin](/uploads/picard-plugin.png)

With the plugin installed:

1. Follow the [quick-start instructions](https://picard.musicbrainz.org/quick-start/) to create clusters out of music you wish to contribute.
2. Right-click and select `plugins -> Add Cluster As Release`. This will pull up the contribution form in your browser with tons of information already filled out.
3. Fill in any missing information you can find.
4. Submit the edit.

After a few moments, try re-tagging the album with beets. If everything worked properly, beets should find the release you contributed and finish tagging it. Repeat this process for any albums you wish to add to MusicBrainz.

## Conclusion

You may find that your whole library is already in MusicBrainz and requires no contributions. If you did have to contribute, be proud-- the whole community gets to benefit from your work, and once it's done, the information stays in MusicBrainz forever. Plus, if you find yourself having to tag the same music later, it will already be there!

Either way, beets + MusicBrainz is a powerful combination and allows you to tightly control and easily manage your music library.

---
title: Introduction to Control Groups (cgroups) v2
date: 2020-08-01
authors: ["jallbrit"]
profileSource: gitlab
tags: ["linux", "cli"]
draft: true
---

## What Are Control Groups?

The manual puts it pretty well:

> Control groups, usually referred to as cgroups, are a Linux kernel feature which allow processes to be organized into hierarchical groups whose usage of various types of resources can then be limited and monitored.
>
> --<cite>`man cgroups`</cite>

Put simply, control groups (or cgroups) allow you to limit the resource usage of certain processes on your Linux system. If you've ever used a pesky program that keeps using up all your RAM, control groups can fix that.

## Control Groups Versioning

There's technically two versions of cgroups- the first was introduced to the kernel in 2008, but since then, cgroups was rewritten and re-implemented in 2016. This new version, aptly called cgroups version 2, is a lot different from version 1, so it's important to know which version you're working with. A summary and reasoning of the version 2 changes is available at [kernel.org](https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html#issues-with-v1-and-rationales-for-v2).

By default, most modern systems will be running both version 1 and 2 simultaneously. This can cause interference, so we first want to start by disabling version 1 altogether:

Since each version of cgroups operates differently, it's important to make sure that only version 2 is running on your machine

## Control Group Hierarchy

### The Root cgroup

Initially, only the root cgroup exists, to which all processes belong. The root cgroup is created by mounting a `cgroup2` filesystem to a certain directory on your system. To find where the root cgroup is mounted, use `mount`:

```bash
$ mount -t cgroup2
cgroup2 on /sys/fs/cgroup/unified type cgroup2 (rw,nosuid,nodev,noexec,relatime,nsdelegate)
```

If you're using `systemd`, chances are the root cgroup will be mounted at `/sys/fs/cgroup/unified` like in the example above. Let's check the permissions on that directory using `ls`:

```bash
$ ls -dl /sys/fs/cgroup/unified/
dr-xr-xr-x 7 root root 0 Jan  1 12:01 /sys/fs/cgroup/unified/
```

As you can see above, this directory is owned by the `root` user, so regular users are unable to make changes. Later, we will fix this so that some trusted users are able to utilize cgroups, but for now, you'll need `root` or `sudo` access.

### Parent and Child cgroups

In cgroups v2, all cgroups branch out from the root cgroup like a tree. If a cgroup is a **child cgroup**, that just means it is a branch from its **parent cgroup**. A child cgroup can be created using `mkdir` from inside any other cgroup. It is common for multiple cgroups to be nested, all starting from the root cgroup like so:

```
        / A - X
unified - B
        \ C - Y
            \ Z
```

<figcaption>

In this example, cgroups `A`, `B`, and `C` are child cgroups of the root cgroup, `X` is a child of `A`, and `C` is the parent of both `Y` and `Z`.

</figcaption>

Child cgroups inherit proprieties and limits from their parent cgroup. For example, if cgroup `C` was given a hard limit of 1000 MB of memory, then groups `Y` and `Z` (plus any child cgroups they may contain) can never use more than 1000 MB combined. Furthermore, you could place another limit so that cgroup `Y` can only use 40% of those 1000 MB and cgroup `Z` gets the rest.

> Remember: child cgroups exist as a sub-directory of their parent cgroup.

## Create a Control Group

For this tutorial, we will create a cgroup called `myGroup` that is a child of the root cgroup:

```bash
$ cd /sys/fs/cgroup/unified
$ mkdir myGroup
```

Poof! Your system will automatically populate this new sub-directory with the proper cgroup files:

```bash
$ cd myGroup
$ ls
cgroup.controllers  cgroup.max.depth        cgroup.stat             cgroup.type   io.pressure
cgroup.events       cgroup.max.descendants  cgroup.subtree_control  cpu.pressure  memory.pressure
cgroup.freeze       cgroup.procs            cgroup.threads          cpu.stat
```

## Controllers

Just like their name, each cgroup **controller** is responsible for *controlling* a specific system resource. There are currently 4 controllers available in cgroups v2:

| Controller | Usage |
|------------|-------|
`cpu` | regulates distribution of CPU cycles
`io` | regulates distribution of IO resources
`memory` | regulates distribution of memory
`pids` | limits the maximum amount of processes in a cgroup

<figcaption>Some useful controllers.</figcaption>

## Enabling A Control Group's Controllers

### 1. Check Which Controllers are Available

Each cgroup contains a `cgroup.controllers` file which lists the controllers that are available for the cgroup to use: 
```bash
$ cat cgroup.controllers
cpu memory pids
```

This list of available controllers comes straight from the parent cgroup. If the parent cgroup doesn't have a particular controller enabled, then that controller will not be listed in `cgroup.controllers`.

> If you don't see *any* available controllers, even in the root cgroup, it's probably because your machine is still [partially using cgroups v1](https://serverfault.com/a/931180).

### 2. Enable/Disable Controllers

To actually enable or disable the controllers, assuming the controller is listed in `cgroup.controllers`, you must write to a file named `cgroup.subtree_control` like so:

```bash
$ echo "-cpu +memory +pids" > cgroup.subtree_control
```

<figcaption>A <code>+</code> and <code>-</code> will enable and disable controllers, respectively.</figcaption>

> Remember: you cannot enable controllers that are not present in `cgroup.controllers`.

### Example

To visualize how this resource controlling works, let's take a look at an example sub-hierarchy of cgroups (enabled controllers are in parentheses):

```
A(cpu, memory) - B(cpu) - C()
                        \ D()
```

In this example, cgroup `A` can control the `cpu` and `memory` usage of cgroup `B`. However, since cgroup `B` only has `cpu` enabled, cgroup `B` can only control the `cpu` (and *not* `memory`) usage of child cgroups `C` and `D`. Furthermore, it would be impossible for cgroups `C` and `D` to enable the `memory` controller, since their parent cgroup `B` does not have the `memory` controller enabled.

## Controlling Resources

### Resource Distribution Models

There are multiple methods to control the distribution of a resource. You may want to set a *limit* to how much memory a cgroup can use, or you might want to *gurantee* that the cgroup always has a certain amount of memory to work with, or you might want to *split* a resource between children. These methods are called **Resource Distribution Models**.

Model | Description | Interface File Example
------|-------------|-----------------------
Weights | Each child is given a percentage of the resource equal to that child's weight divided by the sum of all the children's weight[^weights] | `cpu.weight`
Limits | Child can use the resource up to the limit | `io.max`
Protections | Gurantees[^gurantee] a child has access to a minimum amount of a resource | `memory.low`
Allocations | Child is allocated a certain amount of a resource | `cpu.rt.max`

> Protections and Allocations can be either *hard* or *soft*. *Soft* means the system tries using its "best effort" to control the resource as desired, whereas *hard* means the system aggressively enforces the rules you set.

[^weights]: For example, if child `A` has weight 100 and child `B` has weight 50, child `A` will recieve `100 / (100 + 50) = 66%` of the resource, and child `B` will receive `50 / (100 + 50) = 33%` of the resource. 

[^gurantee]: Resource protections are not always gurantees; sometimes the system will fail to provide a minimum amount of a resource.

### Interface Files

**Interface files** are how you interact with a cgroup's controllers. Each controller has a set of interface files that it reads to determine how it should distribute its resource.

Here is a list of some useful interface files:

File         | Model   | Purpose
-------------|---------|--------
`cpu.weight` | Weight | Set CPU weight
`memory.low` | Protection (soft) | Try to gurantee minimum amount of memory
`memory.min` | Protection (hard) | Gurantee[^gurantee] minimum amount of memory
`memory.high` | Limit (soft) | Try to limit memory usage
`memory.max` | Limit (hard) | Gurantee[^gurantee] to limit memory usage to max bytes

## Assigning Processes to a cgroup

> Without any assigned processes, cgroups do nothing.

Each cgroup keeps a list of assigned process PIDs in `cgroup.procs`. You can add a process to a cgroup by adding its PID to this `cgroup.procs` file. For example, this command will find the PID of a `firefox` process and add it to a cgroup called `myGroup`:

```bash
$ echo $(pgrep firefox) >> /sys/fs/cgroup/unified/myGroup/cgroup.procs
```

Note that all processes are automatically assigned to at least 1 cgroup. If you know the process's PID (you can use `pgrep` to find this), you can check a process's cgroup membership by reading the process's cgroup file:

```bash
$ cat /proc/$PID/cgroup
```

### Remove a cgroup

To remove a cgroup, it needs to be fully unused:

1. Ensure the cgroup doesn't contain any processes: either `cat myGroup/cgroup.procs` and see nothing, or `cat myGroup/cgroup.events` and see `populated 0`.
2. Ensure the cgroup doesn't contain any child cgroups (sub-directories): check the output of `ls myGroup`, or run `find myGroup/ -type d` to see any existing sub-directories

To actually remove the cgroup, use `rmdir`:

```
$ rmdir myGroup
```

> Note: technically, `rmdir` is only supposed to work on empty directories, but this is a special case.

## Conclusion

Control groups are not an easy feature to understand or make full use of, however they allow limitless control over your system resources. Whether you're running a Linux server or just want to cap a pesky program's RAM usage, cgroups are certainly handy.

For a more in-depth guide to Control Groups, just read the cgroups manual:

```bash
$ man cgroups
```

---
title: Fixing Suckless Terminal + Pywal Background Bug
profileSource: gitlab
authors: ["jallbrit"]
tags: ["suckless", "st", "pywal"]
date: 2019-12-12
---

## The Problem

In my ongoing quest to incorporate `pywal`'s colors into all my applications, I came across a bug in [suckless terminal](https://st.suckless.org), AKA `st`.

`pywal` keeps track of 16 terminal colors in addition to an extra, *special* color known as `background`.

However, by default, `st` uses terminal color 0 as the background color. You may not even notice a difference-- in fact, for some of builtin `pywal` colorschemes, terminal color 0 and `background` are equivalent.

## st

Inside the source code of `st` is the culprit of this problem:

<p class="codeblock-label">st/config.h</p>

```c
/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
```

`defaultbg` is set to terminal color 0. However, `pywal` sets the `background` value to index 232:

<p class="codeblock-label">pywal/sequences.py</p>

```python
# Special colors.
# Source: https://goo.gl/KcoQgP
# 10 = foreground, 11 = background, 12 = cursor foregound
# 13 = mouse foreground, 708 = background border color.
sequences.extend([
set_special(10, colors["special"]["foreground"], "g"),
set_special(11, colors["special"]["background"], "h", alpha),
set_special(12, colors["special"]["cursor"], "l"),
set_special(13, colors["special"]["foreground"], "l"),
set_special(17, colors["special"]["foreground"], "l"),
set_special(19, colors["special"]["background"], "l"),
set_color(232, colors["special"]["background"]),
set_color(256, colors["special"]["foreground"])
])
```

Therefor, in order to fix the mismatch, simply change `defaultbg` in `st/config.h` to 232.

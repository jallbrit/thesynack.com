---
title: Remap Caps-Lock (to Escape, Control, etc.) Persistently
date: 2021-02-01
profileSource: gitlab
authors: ["jallbrit"]
tags: ["linux"]
---

## Why Remap Caps Lock?

For whatever reason you use a computer, efficiency is *key* (pun intended). There's one key on your keyboard in prime real estate that's notorious for being accidentally pressed: Caps Lock.

The Caps Lock key is extremely easy to press (on accident or purpose) and is basically wasted on unwanted behavior. It makes more sense to remap caps lock to a more important key, like control (most people) or escape (me, a vim user).

## Simple (Non-Persistent) Remapping Using `setxkbmap`/`xcape`

There are several utilities that allow you to easily remap caps lock. For example, `setxkbmap` is likely present on your machine, and you can just run:

```bash
$ setxkbmap -option caps:escape
```

In addition, there's `xcape`:

```bash
$ xcape -e 'Caps_Lock=Escape'
```

However, none of these solutions are *persistent*: when you reboot your computer, wake from suspend, or just unplug/replug your external keyboard, these tools must be ran once again.

Some people resort to binding a hotkey *just* to run these commands, or they create a `udev` rule, or `systemctl` service, but there's an easier way.

## Let's Get Persistent

On Linux, there is a file called `/etc/default/keyboard`:

> The keyboard file describes the properties of the keyboard.
>
> --<cite>`man 5 keyboard`</cite>

How insightful. Let's keep reading:

> Description of all available keyboard models, layouts, variants and options is available in /usr/share/X11/xkb/rules/xorg.lst

### Pick Your Desired Behavior

OK, let's check `/usr/share/X11/xkb/rules/xorg.lst` and see what options we can find about `caps lock`:

```bash
$ grep "caps" /usr/share/X11/xkb/rules/xorg.lst

  [OUTPUT TRIMMED]

  ctrl:nocaps          Caps Lock as Ctrl
  ctrl:swapcaps        Swap Ctrl and Caps Lock
  ctrl:swapcaps_hyper  Caps Lock as Control, Control as Hyper

  [OUTPUT TRIMMED]

  caps:swapescape      Swap ESC and Caps Lock
  caps:escape          Make Caps Lock an additional Esc
  caps:escape_shifted_capslock Make unmodified Caps Lock an additional Esc, but Shift + Caps Lock behaves like regular Caps Lock
  caps:super           Make Caps Lock an additional Super
  caps:ctrl_modifier   Caps Lock is also a Ctrl
```

As you can see, there's a lot of options (and a lot more I trimmed from the output). I really liked the sound of `caps:escape_shifted_capslock`, but you can pick any of these options.

### Implement the Changes

Now, open `/etc/default/keyboard`. You should see something like this:

<div class="codeblock-label">/etc/default/keyboard</div>

```
# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT=""
XKBOPTIONS=""

BACKSPACE="guess"
```

Change the line that says `XKBOPTIONS=""` to contain the option you desire. For example, I put:

```
XKBOPTIONS="caps:escape_shifted_capslock"
```

Now hit caps lock and... wait, it didn't do anything. Let's check the man page again:

> In Debian systems, changes in /etc/default/keyboard do not become immediately visible to X. You should either reboot the system, or use
>
>     udevadm trigger --subsystem-match=input --action=change
>
> --<cite>`man 5 keyboard`</cite>

To incorporate our changes, run the command it specifies:

```bash
$ udevadm trigger --subsystem-match=input --action=change
```

## Conclusion

After a few seconds, you're good to go! If you liked this post, make sure to **smash** that caps lock button. Hopefully, it did something different.

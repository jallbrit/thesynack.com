---
title: Newsletter
---

# Introducing theSynAck Newsletter

## Content

If you enjoy the type of content featured on theSynAck, subscribe to our newsletter for even more! Here's some examples of topics you'll probably see on most issues:

* Free and Open-Source Software
* Linux (all distributions)
* Cybersecurity (defensive & offensive)

Articles are chosen from across the web, and are not limited to any particular website or topic.

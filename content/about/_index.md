I love all things tech, but on this blog you'll mostly find posts related to:

* FOSS (Free & Open-Source Software)
* GNU/Linux
* Web Development
* Cybersecurity

Hopefully, you find something that helps you!

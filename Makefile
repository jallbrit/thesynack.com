# add node_modules to $PATH
PATH	:= $(PATH):node_modules/.bin

.PHONY: all
all: static/css/bundle.css

.PHONY: clean
clean:
	rm -r static/css

# build css
static/css/bundle.css: $(wildcard src/css/*.css) tailwind.config.js postcss.config.js | static
	postcss src/css --dir static/css
	cat static/css/* > static/css/bundle.css

static:
	mkdir -p static/css
	mkdir -p static/js

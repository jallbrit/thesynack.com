<div align="center">
	<a href="https://thesynack.com" target="_blank">
		<img width="200" src="/static/uploads/logo.svg">
		<br>
		<img width="300" src="/static/uploads/logo-text.svg">
	</a>
	<br>
	<p>Home of <em>byte-sized</em> articles!</p>

![Website](https://img.shields.io/website?url=https%3A%2F%2Fthesynack.com)
![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/jallbrit/thesynack.com/master)

</div>

## About

[theSynAck](https://thesynack.com) is an open source tech blog. All content is written in markdown and rendered using the [static site generator](https://staticgen.com) [Hugo](https://gohugo.io).

This blog is hosted on GitLab Pages using Gitlab's simple CI/CD. Whenever changes are pushed to this repo, the website is automatically rebuilt and deployed.
